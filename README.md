# naala89/bookdown-rsync

This image generates a static from markdown with the capability to  upload to a
remote location.

## Getting Started

These instructions will cover usage information and for the docker container

### Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

### Usage

#### Environment Variables

* ```CSS_BOOTSWATCH``` - (optional) the bootswatch style
* ```CSS_PRISM``` - (optional) the Prism style.
* ```MENU_LOGO``` - (optional) path to the logo for the header.

For more details, see [sandrokeil/bookdown][sandrokeil_bookdown]

#### Volumes

* ```/app``` - Source markdown to be parsed by Bookdown.

#### Useful File Locations

* ```/composer/vendor/bin/bookdown``` - Location of the bookdown binary.

#### Example

Run interactively within the container.

```shell
docker run -it --rm -v .:/app naala89/apiopenstudio-bookdown:latest /bin/sh
$ CSS_BOOTSWATCH="spacelab"
$ CSS_PRISM="prism"
$ MENU_LOGO="/img/my_logo.png"
$ /composer/vendor/bin/bookdown /app/bookdown.json
$ rsync -rvI --delete --exclude=".*" "/site/" "user@my_site:/var/www/my_site"
$ ssh "user@my_site" "sudo chown -R www-data:www-data /var/www/my_site/*"
```

Run Bookdown within the container (no rsync)````

```shell
docker run --rm -v .:/app -e CSS_BOOTSWATCH="spacelab" -e CSS_PRISM="prism" -e \
MENU_LOGO="/img/my_logo.png" naala89/apiopenstudio-bookdown:latest \
bookdown.json
```````

## Find Us

* [GitLab][gitlab_apiopenstudio]
* [GitHub][github_naala89]
* [www.apiopenstudio.com][web_apiopenstudio]

## Versioning

The unstable tags are tagged with the Gitlab Pipeline ID.

Version tags (i.e. ```bookdown-rsync:3.1```) references the
version of the base image.

The ```latest``` tag is master branch, his will be based on the most recent tag.

## Authors

* **John Avery** - [John - GitLab][gitlab_john]

## License

This project is licensed under the MIT License - see the
[LICENSE.md][license] file for details.

## Acknowledgments

Many thanks to:

* [Sandrokeil][sandrokeil] for creating the
* [sandrokeil/bookdown][sandrokeil_bookdown] base image this uses.
* The contributors to [Bookdown.io][bookdown] for a great documenting tool.

[docker_windows]: https://docs.docker.com/windows/started
[docker_osx]: https://docs.docker.com/mac/started/
[docker_linux]: https://docs.docker.com/linux/started/
[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio
[gitlab_john]: https://gitlab.com/john89
[github_naala89]: https://github.com/naala89
[web_apiopenstudio]: https://www.apiopenstudio.com
[sandrokeil]: https://hub.docker.com/u/sandrokeil
[sandrokeil_bookdown]: https://hub.docker.com/r/sandrokeil/bookdown
[bookdown]: http://bookdown.io/
[license]: https://gitlab.com/apiopenstudio/docker_images/bookdown/-/blob/master/LICENSE.md
